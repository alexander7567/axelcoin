Sample configuration files for:
```
SystemD: axelcoind.service
Upstart: axelcoind.conf
OpenRC:  axelcoind.openrc
         axelcoind.openrcconf
CentOS:  axelcoind.init
macOS:    org.axelcoin.axelcoind.plist
```
have been made available to assist packagers in creating node packages here.

See doc/init.md for more information.
