Building Axelcoin
================

See doc/build-*.md for instructions on building the various
elements of the Axelcoin Core reference implementation of Axelcoin.
