Gitian building
================

This file was moved to [the Axelcoin Core documentation repository](https://github.com/axelcoin-core/docs/blob/master/gitian-building.md) at [https://github.com/axelcoin-core/docs](https://github.com/axelcoin-core/docs).
