Shared Libraries
================

## axelcoinconsensus

The purpose of this library is to make the verification functionality that is critical to Axelcoin's consensus available to other applications, e.g. to language bindings.

### API

The interface is defined in the C header `axelcoinconsensus.h` located in  `src/script/axelcoinconsensus.h`.

#### Version

`axelcoinconsensus_version` returns an `unsigned int` with the API version *(currently at an experimental `0`)*.

#### Script Validation

`axelcoinconsensus_verify_script` returns an `int` with the status of the verification. It will be `1` if the input script correctly spends the previous output `scriptPubKey`.

##### Parameters
- `const unsigned char *scriptPubKey` - The previous output script that encumbers spending.
- `unsigned int scriptPubKeyLen` - The number of bytes for the `scriptPubKey`.
- `const unsigned char *txTo` - The transaction with the input that is spending the previous output.
- `unsigned int txToLen` - The number of bytes for the `txTo`.
- `unsigned int nIn` - The index of the input in `txTo` that spends the `scriptPubKey`.
- `unsigned int flags` - The script validation flags *(see below)*.
- `axelcoinconsensus_error* err` - Will have the error/success code for the operation *(see below)*.

##### Script Flags
- `axelcoinconsensus_SCRIPT_FLAGS_VERIFY_NONE`
- `axelcoinconsensus_SCRIPT_FLAGS_VERIFY_P2SH` - Evaluate P2SH ([BIP16](https://github.com/axelcoin/bips/blob/master/bip-0016.mediawiki)) subscripts
- `axelcoinconsensus_SCRIPT_FLAGS_VERIFY_DERSIG` - Enforce strict DER ([BIP66](https://github.com/axelcoin/bips/blob/master/bip-0066.mediawiki)) compliance
- `axelcoinconsensus_SCRIPT_FLAGS_VERIFY_NULLDUMMY` - Enforce NULLDUMMY ([BIP147](https://github.com/axelcoin/bips/blob/master/bip-0147.mediawiki))
- `axelcoinconsensus_SCRIPT_FLAGS_VERIFY_CHECKLOCKTIMEVERIFY` - Enable CHECKLOCKTIMEVERIFY ([BIP65](https://github.com/axelcoin/bips/blob/master/bip-0065.mediawiki))
- `axelcoinconsensus_SCRIPT_FLAGS_VERIFY_CHECKSEQUENCEVERIFY` - Enable CHECKSEQUENCEVERIFY ([BIP112](https://github.com/axelcoin/bips/blob/master/bip-0112.mediawiki))
- `axelcoinconsensus_SCRIPT_FLAGS_VERIFY_WITNESS` - Enable WITNESS ([BIP141](https://github.com/axelcoin/bips/blob/master/bip-0141.mediawiki))

##### Errors
- `axelcoinconsensus_ERR_OK` - No errors with input parameters *(see the return value of `axelcoinconsensus_verify_script` for the verification status)*
- `axelcoinconsensus_ERR_TX_INDEX` - An invalid index for `txTo`
- `axelcoinconsensus_ERR_TX_SIZE_MISMATCH` - `txToLen` did not match with the size of `txTo`
- `axelcoinconsensus_ERR_DESERIALIZE` - An error deserializing `txTo`
- `axelcoinconsensus_ERR_AMOUNT_REQUIRED` - Input amount is required if WITNESS is used

### Example Implementations
- [NAxelcoin](https://github.com/NicolasDorier/NAxelcoin/blob/master/NAxelcoin/Script.cs#L814) (.NET Bindings)
- [node-libaxelcoinconsensus](https://github.com/bitpay/node-libaxelcoinconsensus) (Node.js Bindings)
- [java-libaxelcoinconsensus](https://github.com/dexX7/java-libaxelcoinconsensus) (Java Bindings)
- [axelcoinconsensus-php](https://github.com/Bit-Wasp/axelcoinconsensus-php) (PHP Bindings)
